model in "/Users/muellerp/Documents/material ongoing projects/ICL-GNSS 2015/IEEEtran/Matlab files/files for publishing/matlab/fitST.txt"
data in jagsdata.R
compile, nchains(1)
parameters in jagsinit2.R
initialize
update 1000
monitor set lambda, thin(1)
monitor set xi, thin(1)
monitor set sigma2, thin(1)
monitor set nu, thin(1)
monitor set y.pred, thin(1)
update 10000
coda *, stem('CODA2')
