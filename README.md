Source codes for reproducing the numerical experiments in P. Muller and R. Piche (2015), An Expectation-Maximisation algorithm for statistical trilateration with skew-t noise.


# Authors #

Philipp Müller <philipp.muller@tut.fi> (joint work with Robert Piche)

# Overview #

This repository contains the Matlab codes to reproduce the results in the paper

* P. Müller and R. Piche (2015). Statistical Trilateration with Skew-t Errors. In Proceedings of International Conference on Localization and GNSS 2015. ([PDF] -- link to PDF will be added when available))

The software is distributed under the GNU General Public Licence (version 2 or later); please refer to the file  Licence.txt, included with the software, for details.

# Instructions for use #

Please proceed as follows:

* Clone the git repository to some local directory. The Matlab codes can be found in the **matlab/** subfolder. If you wish not use the git, you can also download the repository contents via the **Download**-link on the left.

* Start Matlab and change the current directory to the **matlab/** subfolder in the repository. In order to generate Figures 2-4 and the contents of Tables I-II you need to add subfolder **jagstmp/** JAGS under **Set path ..**

* To generate Figure 1 in the paper above, issue the following command at Matlab prompt:
```
#!matlab
   plot_skew_t
```

* To generate Figure 2 and error statistics from Table I, issue the following command:
```
#!matlab
   simulations
```

* To generate Figure 3 and parameter estimates from Table II, issue the following command:
```
#!matlab
   fitting_ST_VB(T_0,T)
```
where T_0 is the number of "burn-in" samples and T the number of retained samples.

* To generate Figure 4, issue the following command:
```
#!matlab
   fitting_ST_VC
```

After running the above commands, you should find the generated eps-files in the current directory (which should be the subfolder **matlab/**).

# Bug tracking and commenting #

To report a bug or formally suggest improvements, please use the **Issues**-link on the left. You can also find some more information and give free-form comments on the page behind the **Wiki**-link on the left.