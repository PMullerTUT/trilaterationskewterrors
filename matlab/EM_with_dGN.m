% Expectation maximisation for additive skew t-distributed measurement
% noise using descending Gauss-Newton steps for updating the parameter 
% estimate.
% 
% INPUT:
%   nEM     number of iterations in expectation maximisation
%   nEM     number of iterations in descending Gauss-Newton
%   nu      degrees of freedom
%   varM    t measurement variance
%   Tk      t shape parameter
%   C       matrix with coordinates of corners
%   y       vector of range measurements
%   m0      prior position estimate
%   P0      covariance matrix of prior position estimate
%   nk      dimension of a single measurement
%
% OUTPUT:
%   m       posterior position estimate
%
% This software is distributed under the GNU General Public 
% Licence (version 2 or later); please refer to the file 
% Licence.txt, included with the software, for details.
%
% Copyright (C) 2015 Philipp Muller

function m = EM_with_dGN(nEM,nGN,xi,del,sig2,nu,C,y,m0,P0,maxl)

nm = length(y);
m = m0';
P = P0;
h = zeros(nm,1);
hm = zeros(nm,1);
H = zeros(nm,2);
R = zeros(nm);

% 1. initialize samples z_k and lambda_k
T = repmat(-xi/del,nm,1);
Tau = ones(nm,1);

nom1 = sig2*(1-del^2)*nu^2;         % used in conditional distr. of tau_k
den2 = 4*sig2*(1-del^2);            % used in conditional distr. of tau_k
nom2 = sig2*(1-del^2);              % used in conditional distr. of t_k
yb = y-xi;
sig2m = nom2;

for j = 1:nEM
    ym = yb - del*T;                % modified measurements
    
    % 2. update x^{(j)} using descending Gauss-Newton steps
    for jj = 1:nGN
        % compute measurement function and its Jacobian
        for k = 1:nm
            h(k) = norm(m-C(:,k),2);
            H(k,:) = (m-C(:,k))' ./ h(k);
            R(k,k) = sig2m / Tau(k);
        end
    
        % compute Gauss-Newton step
        if det(P) < 10e20
            K = P*H' / (R +H*P*H');
            d_GN = m0' - m + K * (ym - h - H*m0' + H*m);
        else
            d_GN = - (H'/R*H) \ H' / R * (h-ym);
        end
        
        % update Gauss-Newton step by iterating alpha
        alpha = 1;
        phi_xh = 0.5*(h-ym)'/R*(h-ym) + 0.5 * (m-m0')' / P0 * (m-m0');
        phi_xmd = phi_xh+1; nl = 0;
        while (phi_xmd > phi_xh) && (maxl > nl)
            m_md = m + alpha*d_GN;
            for k = 1:nm
                hm(k) = norm(m_md-C(:,k),2);
            end
            phi_xmd = 0.5*(hm-ym)'/R*(hm-ym) + 0.5 * (m-m0')' / P0 * (m-m0');
            alpha = 0.5 * alpha;
            nl = nl+1;
        end
        
        % update posterior position estimate
        m = m_md;
    end
    
	% 3. compute tau_k^{(j)} according to (12a)
    den1 = yb-hm-del*T;
    Tau = nom1 ./ (den1.^2 + den2);
    
    % ... and t_k^{(j)}  according to (12b)
    s_TG = sqrt(nom2 ./ (2*Tau));
    c_TG = ((yb-hm) ./ (2*del));
%     T = c_TG + normpdf(-c_TG./s_TG).*s_TG ./ (1-normcdf(-c_TG./s_TG));
    T = c_TG + exp(-.5*log(2*pi) - .5*(-c_TG./s_TG).^2 - log(normcdf(-c_TG./s_TG,'upper'))).*s_TG;
end

end
