% Script for the fitting in Subsection V.C
%
% This software is distributed under the GNU General Public 
% Licence (version 2 or later); please refer to the file 
% Licence.txt, included with the software, for details.
%
% Copyright (C) 2015 Philipp Muller

clear all
close all

rng(0);                         % initialise random number generator

%% Load UWB NLOS data
load('rangeerr.mat')
K = length(Z);

% plot density histogram of generated data
width = 10;                    % width of bins
maxZ = max(Z) / width;
minZ = min(Z) / width;
edges = floor(minZ)*width:width:ceil(maxZ)*width;
counts = histc(Z,edges);
heights = counts/length(Z)/width;
cb = edges+width/2;            % centers of bins
figure()
bar(cb(1:end-1),heights(1:end-1),'hist')
hold on
xlabel('ranging error [cm]');
ylabel('probability');

%% Fitting parameters of skew-t distribution to data using JAGS
% Define some MCMC parameters for JAGS
nchains  = 2;           % How many chains?
nburnin  = 1000;        % How many burn-in samples?
nsamples = 10000;       % How many retained samples?

% Set initial value for latent variable in each chain
initval(1).lambda=0; initval(2).lambda=1;
initval(1).p=10; initval(2).p=20;
initval(1).xi=0;    initval(2).xi=0;
initval(1).nu=5;    initval(2).nu=5;

data.y = Z;
data.n = K;

% Call JAGS to sample
disp( 'Running JAGS...');
tic
[samples, stats, structArray] = matjags( ...
	data, ...                           % Observed data and constants   
    fullfile(pwd, 'fitST.txt'), ...     % File with model definition
    initval, ...                        % Initial values latent variables
    'doparallel',~isempty(gcp('nocreate')), ...  % Parallelization flag
    'nchains', nchains,...              % Number of MCMC chains
    'nburnin', nburnin,...              % Number of burn-in steps to discard
    'nsamples', nsamples, ...           % Number of samples to retain
    'thin', 1, ...                      % Thinning parameter
    'dic', 0, ...                       % Compute the DIC?
    'monitorparams', {'lambda','xi','sigma2','nu','y.pred'}, ...  % List of latent variables to monitor
    'savejagsoutput' , 1 , ...          % Save command line output produced by JAGS?
    'verbosity' , 1 , ...               % 0=no output; 1=minimal output; 2=maximum output
    'cleanup' , 1 );                    % clean up temporary files?
toc

 % Print summary statistics of the posterior distribution for Table II
 disp('node    5prctile    median      95prctile')
 disp(['lambda   ', ...
    num2str([stats.ci_low.lambda,stats.median.lambda,stats.ci_high.lambda],'%12.3e')]);
disp(['xi      ', ...
    num2str([stats.ci_low.xi,stats.median.xi,stats.ci_high.xi],'%12.3e')]);
disp(['sigma2   ', ...
	num2str([stats.ci_low.sigma2,stats.median.sigma2,stats.ci_high.sigma2],'%12.3e')]);
disp(['nu   ', ...
	num2str([stats.ci_low.nu,stats.median.nu,stats.ci_high.nu],'%12.3e')]);

%% Plot pdf of skew-t using median parameters
xi = stats.median.xi;           % location parameter
sig2 = stats.median.sigma2;
sig = sqrt(sig2);
lambda = stats.median.lambda;    % skewness parameter
nu = stats.median.nu;

% Z = edges(1):.2:edges(end);
Z = edges(1):5:edges(end);
nuv = nu*ones(1,length(Z));

P = (2/sig) .* tpdf((Z-xi)./sig,nuv) .* tcdf(lambda*(Z-xi)./sig...
            .*sqrt((nu+1)./(nu+(Z-xi).^2./sig2)),nuv+1);
plot(Z,P,'r-','LineWidth',2);

saveas(gcf,'fit_ST_UWB','epsc')
