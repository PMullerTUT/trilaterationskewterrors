% Plot skew t-distribution using pdf (5) from the paper
%
% This software is distributed under the GNU General Public 
% Licence (version 2 or later); please refer to the file 
% Licence.txt, included with the software, for details.

% Copyright (C) 2015 Philipp Muller

clear all
close all

xi = 0;             % location parameter
sig2 = 1;           % scale parameter
sig = sqrt(sig2);
lambda = [0 1 3];   % skewness parameter
nu = 3;             % degrees of freedom

Z = -5:.1:5;
nuv = nu*ones(1,length(Z));

figure()
P = (2/sig) .* tpdf((Z-xi)./sig,nuv) .* tcdf(lambda(1)*(Z-xi)./sig...
            .*sqrt((nu+1)./(nu+(Z-xi).^2./sig2)),nuv+1);
plot(Z,P,'r-','LineWidth',2);
hold on
clear P
P = (2/sig) .* tpdf((Z-xi)./sig,nuv) .* tcdf(lambda(2)*(Z-xi)./sig...
            .*sqrt((nu+1)./(nu+(Z-xi).^2./sig2)),nuv+1);
plot(Z,P,'b--','LineWidth',2);
clear P
P = (2/sig) .* tpdf((Z-xi)./sig,nuv) .* tcdf(lambda(3)*(Z-xi)./sig...
            .*sqrt((nu+1)./(nu+(Z-xi).^2./sig2)),nuv+1);
plot(Z,P,'k-.','LineWidth',2);
clear P

xl = xlabel('z');
yl = ylabel('probability');
h_leg = legend('la = 0.','la = 1.','la = 3.','Location','NorthWest');
set(h_leg,'FontSize',14);
set(xl,'FontSize',14);
set(yl,'FontSize',14);

saveas(gcf,'plot_ST','epsc')

