% Descending Gauss-Newton algorithm assuming additive zero-mean noise.
% 
% INPUT:
%   nGN     number of iterations in descending Gauss-Newton
%   nu      degrees of freedom
%   varM    t measurement variance
%   Tk      t shape parameter
%   C       matrix with coordinates of corners
%   y       vector of range measurements
%   m0      prior position estimate
%   P0      covariance matrix of prior position estimate
%   nk      dimension of a single measurement
%
% OUTPUT:
%   m       posterior position estimate
%
% This software is distributed under the GNU General Public 
% Licence (version 2 or later); please refer to the file 
% Licence.txt, included with the software, for details.
%
% Copyright (C) 2015 Philipp Muller

function m = descendingGN(nGN,xi,varM,del,nu,C,y,m0,P0,maxl)

nm = length(y);
m = m0';
P = P0;
h = zeros(nm,1);
hm = zeros(nm,1);
H = zeros(nm,2);
% use same mean and variance as for the skew-t
g = sqrt(nu)*gamma((nu-1)/2) / (sqrt(pi)*gamma(nu/2));
y = y-xi-sqrt(varM)*g*del;
varM = varM*( nu/(nu-2) - (g*del)^2);
R = varM*eye(nm);

for jj = 1:nGN
    % compute measurement function and its Jacobian
    for k = 1:nm
    	h(k) = norm(m-C(:,k),2);
        H(k,:) = (m-C(:,k))' ./ h(k);
    end
    
    % compute Gauss-Newton step
    if det(P) < 10e20
    	K = P*H' / (R +H*P*H');
        d_GN = m0' - m + K * (y - h - H*m0' + H*m);
    else
    	d_GN = - (H'/R*H) \ H' / R * (h-y);
    end
    
    % update Gauss-Newton step by iterating alpha
    alpha = 1;
    phi_xh = 0.5*(h-y)'/R*(h-y) + 0.5 * (m-m0')' / P0 * (m-m0');
    phi_xmd = phi_xh+1;
    nl = 0;
    while (phi_xmd > phi_xh) && (maxl > nl)
%         phi_xh = 0.5*(h-y)'/R*(h-y) + 0.5 * (m-m0')' / P0 * (m-m0');
        m_md = m + alpha*d_GN;
        for k = 1:nm
        	hm(k) = norm(m_md-C(:,k),2);
        end
        phi_xmd = 0.5*(hm-y)'/R*(hm-y) + 0.5*(m_md-m0')'/P0*(m_md-m0');
        alpha = 0.5 * alpha;
        nl = nl+1;
    end
    
    % update posterior position estimate
    m = m_md;
end

end
