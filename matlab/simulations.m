% Script for the simulations in Section IV.
%
% This software is distributed under the GNU General Public 
% Licence (version 2 or later); please refer to the file 
% Licence.txt, included with the software, for details.
%
% Copyright (C) 2015 Philipp Muller

clear all
close all

dbstop error

%% Problem parameters
nsim = 100;                 % no. of simulations
nm = 3;                     % no. of distance measurements for one corner
K = 4*nm;                   % total no. of measurements
plots = 1;                  % 0 = plots off, 1 = plots on
nop = 50;                   % no. of positions plotted
les = 40;                   % length of the square's sides
m0 = [0 0];                 % prior mean of x
P0 = 100*eye(2);            % prior covariance of x
nk = 1;                     % size of yk (i.e. a single measurement)

% parameters skew t-distribution
xi = 2;                     % location parameter
lam = 3;                    % skewness parameter
sig2 = 9;                   % scale parameter
nu = 3;                     % t dof parameter
del = lam/sqrt(1+lam^2);    % delta

% parameters for algorithms
nEM = 4;                    % number of EM steps 
nGN = 4;                    % number of Gauss-Newton steps 
maxl = 5;                   % max. iterations for finding alpha in des GN

rng(0);                  % initialise random number generator

%% Simulation and estimation
if plots == 1
    figure()
    % triangles for reference nodes
    h1 = plot(20,20,'k^','linewidth',5);
    hold on
    plot(-20,20,'k^','linewidth',5)
    plot(20,-20,'k^','linewidth',5)
    plot(-20,-20,'k^','linewidth',5)
    axis equal
    axis off
    % scale
    plot([24 29],[-24 -24],'k-')
    plot([29 29],[-24.2 -23.8],'k-')
    plot([24 24],[-24.2 -23.8],'k-')
    text(26,-25.5,'5');
end

% preallocation
err_gn=zeros(2,nsim);       % error of descending GN estimate
timeGN = 0;                 % running time of GN estimate
GNerr = nan(1,nsim);        % positioning errors of GN estimate

err_em=zeros(2,nsim);       % error of EM estimate
timeEM = 0;                 % running time of EM estimate
EMerr = nan(1,nsim);        % positioning errors of EM estimate

for isim=1:nsim
    %% Generate data
    y = zeros(K,1);
    x_true = mvnrnd(m0,P0)';    % draw position from prior distribution
    if plots == 1
        if isim <= nop
            h2 = plot(x_true(1),x_true(2),'r.','MarkerSize',18);
        end
    end
    for k = 1:K
        if k <= .25*K
            % for corner (-les/2,-les/2)
            C(:,k) = [-les/2 -les/2]';
        elseif (k > .25*K) && (k <= .5*K)
            % for corner (les/2,-les/2)
            C(:,k) = [les/2 -les/2]';
        elseif (k > .5*K) && (k <= .75*K)
            % for corner (les/2,les/2)
            C(:,k) = [les/2 les/2]';
        else
            % for corner (-les/2,les/2)
            C(:,k) = [-les/2 les/2]';
        end
        d(k) = norm(x_true-C(:,k),2);
        tau = gamrnd(nu/2,nu/2);
        t = abs(sqrt(sig2/tau)*randn(1));
        y(k) = xi + d(k) + del*t + sqrt((1-del^2)*sig2)*randn(1);
    end
    
    %% Evaluate data
    % descending Gauss-Newton
    tic;
    m = descendingGN(nGN,xi,sig2,del,nu,C,y,m0,P0,maxl);
    time = toc;
    timeGN = timeGN + time;
    err_gn(:,isim) = m - x_true;
    GNerr(isim) = norm(x_true-m);
    if plots == 1
        if isim <= nop
            h4 = plot(m(1),m(2),'kx');
            plot([m(1) x_true(1)],[m(2) x_true(2)],'r-')
        end
    end

    % Expectation Maximisation using descending Gauss-Newton
    tic;
    m = EM_with_dGN(nEM,nGN,xi,del,sig2,nu,C,y,m0,P0,maxl);
    time = toc;
    timeEM = timeEM + time;
    err_em(:,isim) = m - x_true;
    EMerr(isim) = norm(x_true-m);
    if plots == 1
        if isim <= nop
            h3 = plot(m(1),m(2),'bo');
            plot([m(1) x_true(1)],[m(2) x_true(2)],'r-')
        end
    end
end

if plots == 1
    legend([h2 h3 h4],'true','EM','dGN..')
    saveas(gcf,'pos_sim_est','epsc')
end

%% Compute error statistics
meanErrGN = nanmean(GNerr);
medianErrGN = nanmedian(GNerr);
Err95GN = quantile(GNerr,0.95);
timeratio1 = 1;

meanErrEM = nanmean(EMerr);
medianErrEM = nanmedian(EMerr);
Err95EM = quantile(EMerr,0.95);
timeratio2 = timeEM / timeGN;

disp('Method & time & Mean & Med & 95\% err');
disp(['dGN & ' num2str(round(timeratio1)) ' & ' num2str(meanErrGN) ...
    ' & ' num2str(medianErrGN) ' & ' num2str(Err95GN)]);
disp(['EM & ' num2str(round(timeratio2)) ' & ' num2str(meanErrEM) ' & ' ...
    num2str(medianErrEM) ' & ' num2str(Err95EM)]);
